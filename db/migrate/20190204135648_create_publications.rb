class CreatePublications < ActiveRecord::Migration[5.2]
  def change
    create_table :publications do |t|
      t.string :Client
      t.text :Annonce
      t.date :Date
      t.string :Facture
    end
  end
end
