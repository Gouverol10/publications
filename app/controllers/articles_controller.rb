class ArticlesController < ApplicationController


  def index
    articles = Article.all;
    render json: {status: 'SUCCESS', message:'Loaded', data:articles},status: :ok
  end

  def show
    article = Article.find(params[:id])
    render json: {status: 'SUCCESS', message:'Loaded', data:article},status: :ok
  end

  def create
    article = Article.create (article_params)

    if article.save
      render json: {status: 'SUCCESS', message:'Saved', data:article},status: :ok
    end
  end


  private
  def article_params
    params.permit( :title, :description, :name)
  end
end